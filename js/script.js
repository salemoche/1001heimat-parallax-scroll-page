
var controller = new ScrollMagic.Controller();
var scene = new ScrollMagic.Scene({triggerElement: "body", duration: 2000})
        // animate color and top border in relation to scroll position
        .setTween(".background", {bottom: "-3000"}) // the tween durtion can be omitted and defaults to 1
        .addIndicators({name: "2 (duration: 300)"}) // add indicators (requires plugin)
        .triggerHook("onLeave")
        .addTo(controller);


var scene = new ScrollMagic.Scene({triggerElement: ".no1", duration: 300})
        // animate color and top border in relation to scroll position
				.setPin(".text1")
        .addIndicators({name: "2 (duration: 300)"}) // add indicators (requires plugin)
        .triggerHook("onLeave")
        .addTo(controller);

// $(document).ready(function() {
//   $('body').scrollTop($(document).height() - window.innerHeight);
//   console.log($(document).height());
// });

// $( window ).load(function() {
//   // Run code
// });
